﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{
    float currentTime = 0f;
    float startingTime = 20f;

    [SerializeField] Text CountdownText;

    void Start()
    {
        currentTime = startingTime;
    }

    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        CountdownText.text = currentTime.ToString("0");

        if (currentTime <= 10)
        {
            CountdownText.color = Color.yellow;
        }

        if (currentTime <= 5)
        {
            CountdownText.color = Color.red;
        }

        if (currentTime <= 4)
        {
            CountdownText.color = Color.white;
        }

        if (currentTime <= 3)
        {
            CountdownText.color = Color.red;
        }

        if (currentTime <= 2)
        {
            CountdownText.color = Color.white;
        }

        if (currentTime <= 1)
        {
            CountdownText.color = Color.red;
        }

        if (currentTime <= 0)
        {
            currentTime = 0;
        }
    }
}