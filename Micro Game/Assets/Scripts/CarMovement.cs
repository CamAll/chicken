﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour {

    public float MoveSpeed;

	// Use this for initialization
	void Start ()
    {
        MoveSpeed = 7.5f;
	}
	
	// Update is called once per frame
	void Update ()
    {
    
        transform.Translate(0f, 0f, (MoveSpeed*Input.GetAxis("Horizontal") * Time.deltaTime));
    }
}
